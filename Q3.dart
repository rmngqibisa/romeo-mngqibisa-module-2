void main() {
  var MTN_App = App();
  MTN_App.disp();
}

// Class
class App {
  String appName = "EasyEquities";
  String appSector = "Finance";
  String yearWon = "2020";
  String appDeveloper = "First World Trader";

// Function
  void disp() {
    print(appName);
    print(appSector);
    print(appDeveloper);
    print(yearWon);
    //part B
    print(appName.toUpperCase());
  }
}
